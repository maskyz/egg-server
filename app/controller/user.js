'use strict';
const ms = require('ms')
const Controller = require('egg').Controller;

class UserController extends Controller {
  async login() {
    const { ctx } = this;

    const { username, password } = ctx.request.body;
    // console.log('sdsds', ctx.request.body)
    const user = {
      username,
      password
    };

    // 设置 Session
    ctx.session.user = user;
    ctx.session.maxAge = ms('30d');
    ctx.body = 'login success';
  }
  async getUserInfo() {
    const { ctx } = this
    ctx.body = {
      success: true,
      data: ctx.session.user
    };
  }
  async register() {
    const { ctx } = this;

    const { username, password } = ctx.request.body;
    const result = await this.app.mysql.insert('user', { username, password })
    // => INSERT INTO `user`(`username`, `password`) VALUES('kang', '123456')
    const isSuccess = result.affectedRows === 1
    ctx.body = {
      success: isSuccess,
      data: isSuccess ? 'success' : 'error'
    }
  }
}

module.exports = UserController;
