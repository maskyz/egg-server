class Request {
  constructor() {
    
  }
  init(method, url, data) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest()
      xhr.open(method, url)
      xhr.send(data)
      xhr.onreadystatechange(e => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            resolve(xhr.responseText)
          } else {
            reject(xhr.responseText)
          }
        }
       })
    })
  }
  get(url, data) {
    return this.init('GET', url, {})
  }
  post(url, data) {
    return this.init('POST', url, data)
  }
}

const request = new Request()