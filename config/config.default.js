/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};

  config.security = {
    csrf: {
      enable: false,
    }
  };

  config.session = {
    renew: true
  }

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1658822943952_2576';

  // add your middleware config here
  config.middleware = [];

  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
  };

  config.mysql = {
    // 单数据库信息配置
  client: {
    // host
    host: 'xx.xx.xxx.xxx',
    // 端口号
    port: 'xxxx',
    // 用户名
    user: 'xxx',
    // 密码
    password: 'xxxxxx',
    // 数据库名
    database: 'xxxx',
  },
  // 是否加载到 app 上，默认开启
  app: true,
  // 是否加载到 agent 上，默认关闭
  agent: false,
  }

  return {
    ...config,
    ...userConfig,
  };
};
